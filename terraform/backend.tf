terraform {
  backend "s3" {
   bucket = "fargate-test1"
   region = "us-east-1"
   key = "test/ecs/terraform.tfstate"
  }
}
